import React, { Component } from 'react';
import axios from 'axios';

const styleInput = {
    width: '200px'
}

const styleButton = {
    width: '150px'
}

class Search extends Component {
    constructor(props) { 
        super(props); 
        this.state = {
            currPlace: "",
            woeid: 0,
            temp: 0,
            weather_state: "",
        }; 
    } 

    handleChange(e){
        e.preventDefault();
        this.setState({ currPlace: e.target.value });
    }

    handleClick(){
        let url = `https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/search/?query=${this.state.currPlace}`
        //console.log("url: " + url);
        axios.get(url).then(data => {
            this.setState(() => ({ 
            woeid: data.data[0].woeid,
            }))
            let url2 = `https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/${this.state.woeid}/`
            //console.log("url2: " + url2);
            axios.get(url2).then(data => {
                this.setState(() => ({ 
                temp: data.data.consolidated_weather[0].the_temp,
                weather_state: data.data.consolidated_weather[0].weather_state_name
            }))

        }).catch(err => console.log(err.message)); //eslint-disable-line
        }).catch(err => console.log(err.message)); //eslint-disable-line
    } 

  render(){
    return (
        <div>
          <div className="container">
            <section className="search six offset-by-three columns">
            <h1> {this.state.currPlace} </h1>
            <h3> {this.state.weather_state} </h3>
            <h3> {this.state.temp} ºC </h3>
                <input
                  className="u-full-width"
                  type="text"
                  name="place"
                  placeholder="Enter city"
                  onChange={ this.handleChange.bind(this) }
                  style= {styleInput}
                />
                <br/>
                <br/>
                <input type="submit" value="Search Weather" onClick={this.handleClick.bind(this)}
                style= {styleButton}/>   
            </section>
          </div>
        </div>
      );
    };
  }
  
export default Search;